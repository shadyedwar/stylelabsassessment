## README

The purpose of this repository is to assess the technical skills of applicants applying for the job as senior developer.

The solution consists of two projects.

The Stylelabs.Recruitment project contains a small data layer providing following functionality:

* Defining a domain model via entity definitions

* Create instances of entity definitions via the entity factory

The Stylelabs.Recruitment.App project in which a simple domain model is created consisting of a Movie and Actor definition.
The project also provides a few examples of how entities are created.
