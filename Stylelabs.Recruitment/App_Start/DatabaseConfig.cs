﻿using Microsoft.Azure.Documents;
using Stylelabs.Recruitment.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.App_Start
{
    /// <summary>
    /// Mangage  collection creation and rigister triggers 
    /// </summary>
    public class DatabaseConfig
    {
        /// <summary>
        /// Create collections and rigister triggers 
        /// </summary>
        /// <param name="context"></param>
        public static void CrateateCollectionsAndRegisterScripts(IContext context)
        {
            string filePath = @".\..\Stylelabs.Recruitment\JS\Triggers\"; // path of java script files;
            string entityDefinitionCollectionId = ConfigurationManager.AppSettings["entityDefinitionCollectionId"];
            if (string.IsNullOrEmpty(entityDefinitionCollectionId))
                throw new ArgumentException("No EntityDefinition Collection Id configured in AppSettings");
            string entityCollectionId = ConfigurationManager.AppSettings["entityCollectionId"];
            if (string.IsNullOrEmpty(entityCollectionId))
                throw new ArgumentException("No Entity Collection Id configured in AppSettings");

            DocumentCollection entityDefinitionCollection = context.ReadOrCreateCollection(entityDefinitionCollectionId); // Create EntityDefinition Collection
            DocumentCollection entityCollection = context.ReadOrCreateCollection(entityCollectionId); // Create Entity Collection

            //Triggers for EntityDefinition Collection
            Trigger PreCreateEntityDefinitionAutoIdentity = new Trigger()
            {
                Id = "PreCreateEntityDefinitionAutoIdentity",
                Body = File.ReadAllText(filePath + "PreCreateEntityDefinitionAutoIdentity.js"),
                TriggerType = TriggerType.Pre,
                TriggerOperation = TriggerOperation.Create
            };
            Trigger PreDeleteEntityDefinitionReferentialIntegrity = new Trigger()
            {
                Id = "PreDeleteEntityDefinitionReferentialIntegrity",
                Body = File.ReadAllText(filePath + "PreDeleteEntityDefinitionReferentialIntegrity.js"),
                TriggerType = TriggerType.Pre,
                TriggerOperation = TriggerOperation.Delete
            };
            Trigger PreUpdateEntityDefinitionReferentialIntegrity = new Trigger()
            {
                Id = "PreUpdateEntityDefinitionReferentialIntegrity",
                Body = File.ReadAllText(filePath + "PreUpdateEntityDefinitionReferentialIntegrity.js"),
                TriggerType = TriggerType.Pre,
                TriggerOperation = TriggerOperation.Replace
            };

            //Register triggers for EntityDefinition Collection
            context.RegisterTriggerAsync(entityDefinitionCollection, PreCreateEntityDefinitionAutoIdentity);
            context.RegisterTriggerAsync(entityDefinitionCollection, PreDeleteEntityDefinitionReferentialIntegrity);
            context.RegisterTriggerAsync(entityDefinitionCollection, PreUpdateEntityDefinitionReferentialIntegrity);


            //Triggers for Entity Collection
            Trigger PreCreateEntityAutoIdentity = new Trigger()
            {
                Id = "PreCreateEntityAutoIdentity",
                Body = File.ReadAllText(filePath + "PreCreateEntityAutoIdentity.js"),
                TriggerType = TriggerType.Pre,
                TriggerOperation = TriggerOperation.Create
            };

            //Register triggers for Entity Collection
            context.RegisterTriggerAsync(entityCollection, PreCreateEntityAutoIdentity);
        }
    }
}
