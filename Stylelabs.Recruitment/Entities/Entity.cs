﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Stylelabs.Recruitment.Framework;
using Newtonsoft.Json;
using Microsoft.Azure.Documents;

namespace Stylelabs.Recruitment.Entities
{


    public class Entity : IDocument, IDirtyTrackable
    {
        #region Fields

        private bool _isDirty;

        #endregion

        #region Properties

        /// <summary>
        /// Indicates if this is a new instance.
        /// A new instance has not been stored in the database.
        /// </summary>
        [JsonIgnore]
        public bool IsNew { get; internal set; }

        /// <summary>
        /// Indicates if this instance is currently tracking changes.
        /// </summary>
        [JsonIgnore]
        public bool IsTracking { get; private set; }

        /// <summary>
        /// Indicates if this instance is dirty.
        /// </summary>
        public bool IsDirty
        {
            get { return _isDirty || Properties.Any(p => p.IsDirty) || Relations.Any(p => p.IsDirty); }
        }

        /// <summary>
        /// String representation of Auto generated I, it will be used by DocumentDB
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string StringId { get { return Id.ToString(); } internal set { } }

        /// <summary>
        /// Gets or sets the unique identifier of the entity.
        /// </summary>
        /// <remarks>
        /// When an object has not been made persistent, the Id is set to 0.
        /// </remarks>
        [JsonProperty(PropertyName = "DId")]
        public long Id { get; internal set; }

        /// <summary>
        /// Gets the id of the <see cref="EntityDefinition"/> of which this entity is an instance.
        /// </summary>
        /// 
       [JsonProperty]
        public long DefinitionId { get; internal set; }

        /// <summary>
        /// Gets the <see cref="EntityDefinition"/> of which this entity is an instance.
        /// </summary>
        [JsonProperty(ReferenceLoopHandling = ReferenceLoopHandling.Serialize, TypeNameHandling = TypeNameHandling.All, ItemTypeNameHandling = TypeNameHandling.All)]
        public virtual EntityDefinition Definition { get; internal set; }

        /// <summary>
        /// Gets the properties that belong to this entity.
        /// </summary>
        [JsonProperty(ReferenceLoopHandling = ReferenceLoopHandling.Serialize, TypeNameHandling = TypeNameHandling.All, ItemTypeNameHandling = TypeNameHandling.All)]
        public IEnumerable<Property> Properties { get; internal set; }

        /// <summary>
        /// Gets the relations that belong to this entity.
        /// </summary>
      //  [ObjectCollectionValidator]
        [JsonProperty(TypeNameHandling = TypeNameHandling.All, ItemTypeNameHandling = TypeNameHandling.All)]
        public IEnumerable<Relation> Relations { get;  internal set; }

        /// <summary>
        /// Indicates whether this instance is built in.
        /// </summary>
        /// <remarks>
        /// Built in entities cannot be removed.
        /// </remarks>
        [JsonProperty]
        public virtual bool IsBuiltIn { get { return false; } }

        /// <summary>
        /// Gets the instance of <see cref="CultureInfo"/> representing the culture that the entity was loaded in.
        /// The values of all properties will be the values for the specified culture.
        /// </summary>
        /// 
        [JsonProperty]
        public CultureInfo Culture { get; internal set; }

        /// <summary>
        /// Id of the user who last modified this entity.
        /// </summary>
        [JsonProperty]
        public long ModifiedBy { get; internal set; }

        /// <summary>
        /// Id of the user who created this entity.
        /// </summary>
        [JsonProperty]
        public long CreatedBy { get; internal set; }

        /// <summary>
        /// The date when this instance was last modified.
        /// </summary>
        [JsonProperty]
        public DateTime ModifiedOn { get; internal set; }

        /// <summary>
        /// The date when this instance was created on.
        /// </summary>
        [JsonProperty]
        public DateTime CreatedOn { get; internal set; }

      

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity" /> class.
        /// </summary>
        public Entity()
        {
            Properties = new List<Property>();
            Relations = new List<Relation>();
            IsNew = true;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Starts tracking changes.
        /// </summary>
        public virtual void StartTracking()
        {
            _isDirty = false;
            IsTracking = true;

            foreach (var prop in Properties)
            {
                prop.StartTracking();
            }

            foreach (var rel in Relations)
            {
                rel.StartTracking();
            }
        }

        /// <summary>
        /// Gets the property with the specified name.
        /// </summary>
        /// <remarks>
        /// When no property with the specified name exists, <c>null</c> is returned.
        /// </remarks>
        /// <typeparam name="T">The generic type of the property to get.</typeparam>
        /// <param name="name">The name of the property to get.</param>
        /// <returns>The concrete <see cref="Property"/> with the specified name.</returns>
        public Property<T> GetProperty<T>(string name)
        {
            Guard.ArgumentNotNullOrEmpty("name", name);

            var result = Properties.FirstOrDefault(p => p.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

            return result == null
                       ? null
                       : (Property<T>)result;
        }

        /// <summary>
        /// Gets the relation with the specified name.
        /// </summary>
        /// <remarks>
        /// When no relation with the specified name exists, <c>null</c> is returned.
        /// </remarks>
        /// <typeparam name="T">The type of the relation to get.</typeparam>
        /// <param name="name">The name of the relation to get.</param>
        /// <returns>The concrete <see cref="Relation"/> with the specified name.</returns>
        public T GetRelation<T>(string name)
            where T : Relation
        {
            Guard.ArgumentNotNullOrEmpty("name", name);

            foreach (var current in Relations)
            {
                if (current.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    var result = current as T;
                    if (result != null) return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the relation with the specified name.
        /// </summary>
        /// <remarks>
        /// When no relation with the specified name exists, <c>null</c> is returned.
        /// </remarks>
        /// <typeparam name="T">The type of the relation to get.</typeparam>
        /// <param name="name">The name of the relation to get.</param>
        /// <param name="role">role of the relation. In case of self referencing relation, every entity will have 2 relations with
        /// varying roles</param>
        /// <returns>The concrete <see cref="Relation"/> with the specified name.</returns>
        public T GetRelation<T>(string name, RelationRole role)
            where T : Relation
        {
            Guard.ArgumentNotNullOrEmpty("name", name);

            return
                Relations.SingleOrDefault(
                    r => r.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase) && r.Role == role) as T;
        }

        public override string ToString()
        {
            return string.Format("{0}:{1}", Definition.Name, Id);
        }

        #endregion

        #region Internal methods

        /// <summary>
        /// Adds the specified property to the list of properties that belong to this entity.
        /// </summary>
        /// <param name="property">The property to add.</param>
        internal void AddProperty(Property property)
        {
            Guard.ArgumentNotNull(property, "property");
            ((List<Property>)Properties).Add(property);
        }

        /// <summary>
        /// Adds the specified relation to the list of relations that belong to this entity.
        /// </summary>
        /// <param name="relation">The relation to add.</param>
        internal void AddRelation(Relation relation)
        {
            Guard.ArgumentNotNull(relation, "relation");
            ((List<Relation>)Relations).Add(relation);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// If the entity is tracking dirtyness, it is marked as dirty.
        /// If the entity is not tracking, nothing happens.
        /// </summary>
        private void MarkAsDirty()
        {
            if (IsTracking)
                _isDirty = true;
        }

        #endregion
    }
}