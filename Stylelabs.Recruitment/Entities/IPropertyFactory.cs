﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Entities
{
    public interface IPropertyFactory
    {
        /// <summary>
        /// Creates a new instance of <see cref="Property" /> for the specified <paramref name="definition" />.
        /// The result will be a concrete implementation of <see cref="Property" />.
        /// </summary>
        /// <param name="definition">The definition to create a property for.</param>
        /// <returns>
        /// A concrete implementation of <see cref="Property" /> for the specified <paramref name="definition" />.
        /// </returns>
        Property Create(PropertyDefinition definition);
    }
}
