﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Entities
{
    /// <summary>
    /// Creates concrete instance of <see cref="Relation"/> based on a specified <see cref="RelationDefinition"/>.
    /// </summary>
    public interface IRelationFactory
    {
        /// <summary>
        /// Creates an instance of <see cref="Relation"/> for the specified <see cref="RelationDefinition"/>.
        /// </summary>
        /// <param name="definition">The definition to create a relation for.</param>
        /// <returns>An instance of <see cref="Relation"/> for the specified <see cref="RelationDefinition"/>.</returns>
        Relation Create(RelationDefinition definition);

        /// <summary>
        /// Creates an instance of <see cref="Relation" /> for the specified <see cref="RelationDefinition" />
        /// and populates the links of the relation with the specified links.
        /// </summary>
        /// <param name="definition">The definition to create a relation for.</param>
        /// <param name="links">The links to populate.</param>
        /// <returns>
        /// An instance of <see cref="Relation" /> for the specified <see cref="RelationDefinition" />.
        /// </returns>
        Relation Create(RelationDefinition definition, IList<long> links);
    }
}
