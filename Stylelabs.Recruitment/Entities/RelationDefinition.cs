﻿using Newtonsoft.Json;
using Stylelabs.Recruitment.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Entities
{
    #region Enums

    public enum RelationCardinality
    {
        /// <exclude/>
        OneToMany,
        /// <exclude/>
        ManyToMany,
        /// <exclude/>
        OneToOne
    }

    public enum RelationRole
    {
        /// <exclude/>
        Parent,
        /// <exclude/>
        Child
    }

    #endregion

    #region RelationDefinition class

    public class RelationDefinition : MemberDefinition
    {
        #region Fields

        private RelationCardinality _cardinality;
        private long _associatedEnityDefinitionId;
        private bool _childIsMandatory;
        private bool _parentIsMandatory;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the role of the <see cref="EntityDefinition"/> in the relation.
        /// </summary>
        [JsonProperty]
        public RelationRole Role { get; internal set; }

        /// <summary>
        /// Gets or sets the cardinality.
        /// </summary>
        [JsonProperty]
        public RelationCardinality Cardinality
        {
            get { return _cardinality; }
            set
            {
                if (_cardinality != value)
                {
                    if (!IsNew)
                        throw new NotSupportedException("Property Cardinality of an existing RelationDefinition cannot be changed.");

                    _cardinality = value;
                    MarkAsDirty();
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the associated <see cref="EntityDefinition"/>.
        /// </summary>
        [JsonProperty]
        public long AssociatedEntityDefinitionId
        {
            get { return _associatedEnityDefinitionId; }
            set
            {
                if (_associatedEnityDefinitionId != value)
                {
                    if (!IsNew)
                        throw new NotSupportedException("Property AssociatedEntityDefinitionId of an existing RelationDefinition cannot be changed.");

                    _associatedEnityDefinitionId = value;
                    MarkAsDirty();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the children are mandatory.
        /// </summary>
       [JsonProperty]
        public bool ChildIsMandatory
        {
            get { return _childIsMandatory; }
            set
            {
                if (_childIsMandatory != value)
                {
                    if (ParentIsMandatory && value)
                        throw new NotSupportedException("A relation cannot have both ChildIsMandatory and ParentIsMandatory set to True.");

                    _childIsMandatory = value;
                    MarkAsDirty();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the parent is mandatory.
        /// </summary>
        [JsonProperty]
        public bool ParentIsMandatory
        {
            get { return _parentIsMandatory; }
            set
            {
                if (_parentIsMandatory != value)
                {
                    if (ChildIsMandatory && value)
                        throw new NotSupportedException("A relation cannot have both ChildIsMandatory and ParentIsMandatory set to True.");

                    _parentIsMandatory = value;
                    MarkAsDirty();
                }
            }
        }

        /// <summary>
        /// Indicates if this instance is dirty.
        /// </summary>
        public override bool IsDirty
        {
            get { return base.IsDirty; }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Creates the inverse relation-definition.
        /// </summary>
        /// <returns></returns>
        public RelationDefinition CreateInverse(long associatedEntityDefinitionId)
        {
            return new RelationDefinition(this)
            {
                AssociatedEntityDefinitionId = associatedEntityDefinitionId,
                Role = Role == RelationRole.Child ? RelationRole.Parent : RelationRole.Child
            };
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RelationDefinition" /> class.
        /// </summary>
        public RelationDefinition()
        {
            Role = RelationRole.Parent;
        }

        public RelationDefinition(RelationDefinition rd)
        {
            var labels = new TrackingDictionary<CultureInfo, string>();
            rd.Labels.Each(p => labels.Add((CultureInfo)p.Key.Clone(), p.Value));

            Name = rd.Name;
            AssociatedEntityDefinitionId = rd.AssociatedEntityDefinitionId;
            Cardinality = rd.Cardinality;
            ChildIsMandatory = rd.ChildIsMandatory;
            ParentIsMandatory = rd.ParentIsMandatory;
            Labels = labels;
            Role = rd.Role;
        }

        #endregion

        #region Overrides

        public override void StartTracking()
        {
            base.StartTracking();
        }

        #endregion

    }

    #endregion
}
