﻿using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    public static class Extensions
    {
        /// <summary>
        /// If the source if different from the default, returns the result of applying the specified projection function to the specified value.
        /// Otherwise, returns the default of the result type.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="projection">The projection function.</param>
        /// <returns>The result of the projection if the source differs from the default.</returns>
        public static TResult Maybe<TSource, TResult>(this TSource source, Func<TSource, TResult> projection)
        {
            if (Equals(default(TSource), source))
            {
                return default(TResult);
            }

            return projection(source);
        }

        /// <summary>
        /// Performs a specified action for each element in the collection.
        /// </summary>
        /// <typeparam name="T">Generic type of the collection.</typeparam>
        /// <param name="collection">The collection to perform the action on.</param>
        /// <param name="action">The action to perform.</param>
        public static void Each<T>(this IEnumerable<T> collection, Action<T> action)
        {
            Guard.ArgumentNotNull(collection, "collection");
            Guard.ArgumentNotNull(action, "action");

            foreach (var item in collection)
            {
                action(item);
            }
        }
    }
}
