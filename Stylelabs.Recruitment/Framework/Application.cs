﻿using Autofac;
using Microsoft.Azure.Documents.Client;
using Stylelabs.Recruitment.App_Start;
using Stylelabs.Recruitment.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    public static class Application
    {
        public static IContainer Container { get; private set; }

        public static void Startup(string databaseId = null)
        {
            string EndpointUrl = ConfigurationManager.AppSettings["EndpointUrl"];
            if (string.IsNullOrEmpty(EndpointUrl))
                throw new ArgumentException("No EndpointUrl configured in AppSettings");

            string AuthorizationKey = ConfigurationManager.AppSettings["AuthorizationKey"];
            if (string.IsNullOrEmpty(AuthorizationKey))
                throw new ArgumentException("No AuthorizationKey configured in AppSettings");

            string entityDefinitionCollectionId = ConfigurationManager.AppSettings["entityDefinitionCollectionId"];
            if (string.IsNullOrEmpty(entityDefinitionCollectionId))

                throw new ArgumentException("No EntityDefinition Collection Id configured in AppSettings");
            string entityCollectionId = ConfigurationManager.AppSettings["entityCollectionId"];
            if (string.IsNullOrEmpty(entityCollectionId))
                throw new ArgumentException("No Entity Collection Id configured in AppSettings");

            DocumentClient client = new DocumentClient(new Uri(EndpointUrl), AuthorizationKey);
            client.OpenAsync().Wait();  

            //Create new autofac container-builder.
            var containerBuilder = new ContainerBuilder();

            //Register all types
            containerBuilder.RegisterType<EntityFactory>().As<IEntityFactory>();
            containerBuilder.RegisterType<PropertyFactory>().As<IPropertyFactory>();
            containerBuilder.RegisterType<RelationFactory>().As<IRelationFactory>();
            containerBuilder.RegisterType<PropertyFactory>().As<IPropertyFactory>();

            containerBuilder.RegisterInstance<DocumentClient>(client).SingleInstance(); //register Singleton instance of DocumentClient 
            containerBuilder.Register<DocumentDBContext>(c => new DocumentDBContext(c.Resolve<DocumentClient>(), databaseId)).As<IContext>().InstancePerDependency();
            containerBuilder.Register<DocumentDBRepository<Entity>>(c => new DocumentDBRepository<Entity>(c.Resolve<IContext>(), entityCollectionId)).As<IRepository<Entity>>();
            containerBuilder.Register<DocumentDBRepository<EntityDefinition>>(c => new DocumentDBRepository<EntityDefinition>(c.Resolve<IContext>(), entityDefinitionCollectionId)).As<IRepository<EntityDefinition>>();
            Container = containerBuilder.Build();

            DatabaseConfig.CrateateCollectionsAndRegisterScripts(Container.Resolve<IContext>());
        }

        
    }
}
