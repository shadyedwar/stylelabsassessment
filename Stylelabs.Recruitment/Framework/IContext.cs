﻿using System;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;

namespace Stylelabs.Recruitment.Framework
{
    /// <summary>
    /// Encapsulate DocumentClient functionality 
    /// </summary>
    public interface IContext
    {
        /// <summary>
        /// Get database
        /// </summary>
        Database Database { get; }

        /// <summary>
        /// Create new database Collection if not exist 
        /// </summary>
        /// <param name="database"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        DocumentCollection ReadOrCreateCollection(string Id);

        /// <summary>
        /// Add new document to a collection
        /// </summary>
        /// <param name="documentCollection"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        Task<T> AddDocumentAsync<T>(DocumentCollection documentCollection, T document, RequestOptions requestOptions=null) where T : IDocument;

        /// <summary>
        /// Get Items/item by linq expression
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IEnumerable<T> GetItem<T>(DocumentCollection documentCollection, System.Linq.Expressions.Expression<Func<T, bool>> predicate) where T : IDocument;

        /// <summary>
        /// Update document
        /// </summary>
        /// <param name="documentCollection"></param>
        /// <param name="document"></param>
        Task UpdateDocumentAsync<T>(string SelfLink, T document, RequestOptions requestOptions = null) where T : IDocument;

        /// <summary>
        /// Delete document
        /// </summary>
        /// <param name="document"></param>
        Task DeleteDocumentAsync<T>(string SelfLink, T document, RequestOptions requestOptions = null) where T : IDocument;

        /// <summary>
        /// Register trigger to a collection 
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="trigger"></param>
        /// <returns></returns>
        Task RegisterTriggerAsync(DocumentCollection collection, Trigger trigger);

        /// <summary>
        /// Delete database by Id if exist
        /// </summary>
        /// <param name="databaseId"></param>
        void DeleteDatabase(string databaseId);

        /// <summary>
        /// Get registered triggers for specific collection
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        IEnumerable<Trigger> GetRegisteredTriggersForCollection(DocumentCollection collection);

        /// <summary>
        ///  Get dynamic Items/item by linq expression, it will be used whenever we need to get a SelfLink for a specific document
        ///  Mainly use in Delete and Update operations
        /// </summary>
        /// <param name="documentCollection"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IEnumerable<dynamic> GetItemDynamic(DocumentCollection documentCollection, System.Linq.Expressions.Expression<Func<Document, bool>> predicate);

    }
}
