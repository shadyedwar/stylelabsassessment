﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    /// <summary>
    /// A scored set associates a number of unique items with scores.
    /// </summary>
    /// <typeparam name="T">Data-type of the set.</typeparam>
    public class ScoredSet<T> : IScoredSet<T>
    {
        #region Fields

        private readonly ISet<T> _innerSet = new HashSet<T>();
        private readonly IDictionary<T, int> _indices = new Dictionary<T, int>();

        #endregion

        #region Properties

        public int Count
        {
            get { return _innerSet.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion

        #region Indexers

        public T this[int index]
        {
            get
            {
                var current = _indices.FirstOrDefault(p => p.Value == index);

                return
                    current.Equals(default(KeyValuePair<T, int>))
                        ? default(T)
                        : current.Key;
            }
            set
            {
                _indices[value] = index;
                _innerSet.Add(value);
            }
        }

        #endregion

        #region Public methods

        public bool Add(T item, int score)
        {
            _indices[item] = score;
            return _innerSet.Add(item);
        }

        public int ScoreOf(T item)
        {
            if (!_indices.ContainsKey(item))
            {
                return -1;
            }
            return _indices[item];
        }

        public void ExceptWith(IEnumerable<T> other)
        {
            _innerSet.ExceptWith(other);
        }

        public void IntersectWith(IEnumerable<T> other)
        {
            _innerSet.IntersectWith(other);
        }

        public bool IsProperSubsetOf(IEnumerable<T> other)
        {
            return _innerSet.IsProperSubsetOf(other);
        }

        public bool IsProperSupersetOf(IEnumerable<T> other)
        {
            return _innerSet.IsProperSupersetOf(other);
        }

        public bool IsSubsetOf(IEnumerable<T> other)
        {
            return _innerSet.IsSubsetOf(other);
        }

        public bool IsSupersetOf(IEnumerable<T> other)
        {
            return _innerSet.IsSupersetOf(other);
        }

        public bool Overlaps(IEnumerable<T> other)
        {
            return _innerSet.Overlaps(other);
        }

        public bool SetEquals(IEnumerable<T> other)
        {
            return _innerSet.SetEquals(other);
        }

        public void SymmetricExceptWith(IEnumerable<T> other)
        {
            _innerSet.SymmetricExceptWith(other);
        }

        public void UnionWith(IEnumerable<T> other)
        {
            _innerSet.UnionWith(other);
        }

        public void Clear()
        {
            _innerSet.Clear();
            _indices.Clear();
        }

        public bool Contains(T item)
        {
            return _innerSet.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _innerSet.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            _indices.Remove(item);
            return _innerSet.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _innerSet.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _innerSet.GetEnumerator();
        }

        #endregion
    }
}
