﻿using System;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    /// <summary>
    /// Provide functionality to deal with  DocumentClient
    /// </summary>
    public class DocumentDBContext : IContext
    {
        #region Fields
        private DocumentClient client;
        private string databaseId;
        private Dictionary<string, DocumentCollection> documentCollection;
        #endregion

        #region Constructors
        public DocumentDBContext(DocumentClient documentClient, string databaseId = null)
        {
            this.databaseId = string.IsNullOrEmpty(databaseId) ? ConfigurationManager.AppSettings["DatabaseId"] : databaseId; //If there is no database Is provided, it will be read from config file
            if (string.IsNullOrEmpty(this.databaseId))
                throw new ArgumentException("No databaseId provided.");
            client = documentClient;
            documentCollection = new Dictionary<string, DocumentCollection>();
            ReadOrCreateDatabase(this.databaseId);
        }
        #endregion

        #region Properties
        public Database Database { get; private set; }
        
        #endregion

        #region Private Methods
        /// <summary>
        /// Get database if it is exist or create new one
        /// </summary>
        /// <param name="databseId"></param>
        private void ReadOrCreateDatabase(string databseId)
        {
            Database = client.CreateDatabaseQuery()
                            .Where(d => d.Id == databaseId)
                            .AsEnumerable()
                            .FirstOrDefault();

            if (Database == null)
            {
                Database = client.CreateDatabaseAsync(new Database { Id = databaseId }).Result;
            }
        }
        /// <summary>
        /// Get collection if it is exist or create new one and cach it in documentCollection dctionary
        /// </summary>
        /// <param name="databaseLink"></param>
        /// <param name="CollectionId"></param>
        /// <returns></returns>
        private DocumentCollection ReadOrCreateCollection(string databaseLink, string CollectionId)
        {
            if (string.IsNullOrEmpty(CollectionId))
                throw new ArgumentNullException("Collection name can't be null or empty");
            var col = client.CreateDocumentCollectionQuery(databaseLink)
                              .Where(c => c.Id == CollectionId)
                              .AsEnumerable()
                              .FirstOrDefault();

            if (col == null)
            {
                var collectionSpec = new DocumentCollection { Id = CollectionId };
                var requestOptions = new RequestOptions { OfferType = "S1" };

                col = client.CreateDocumentCollectionAsync(databaseLink, collectionSpec).Result;
            }

            return col;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get collection from documentCollection dctionary if it is exist or create new one
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public DocumentCollection ReadOrCreateCollection(string Id)
        {
            if (documentCollection.ContainsKey(Id))
            {
                return documentCollection[Id];
            }
            else
            {
                var col = ReadOrCreateCollection(Database.SelfLink, Id);
                documentCollection.Add(Id, col);
                return col;
            }
        }
        #endregion

        #region Implementation of IContext
        /// <summary>
        /// Add new document
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="documentCollection"></param>
        /// <param name="document"></param>
        /// <param name="requestOptions"></param>
        /// <returns></returns>
        public async Task<T> AddDocumentAsync<T>(DocumentCollection documentCollection, T document, RequestOptions requestOptions = null) where T : IDocument
        {
            var response = await client.CreateDocumentAsync(documentCollection.DocumentsLink, document, requestOptions);
            var temp = JsonConvert.DeserializeObject<T>(response.Resource.ToString());
            return temp;
        }
        /// <summary>
        ///  Get dynamic Items/item by linq expression, it will be used whenever we need to get a SelfLink for a specific document
        ///  Mainly use in Delete and Update operations 
        /// </summary>
        /// <param name="documentCollection"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<dynamic> GetItemDynamic(DocumentCollection documentCollection, System.Linq.Expressions.Expression<Func<Document, bool>> predicate)
        {
            return client.CreateDocumentQuery(documentCollection.DocumentsLink)
                   .Where(predicate)
                  .AsEnumerable();
        }

        /// <summary>
        /// Get Items/item by linq expression
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="documentCollection"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<T> GetItem<T>(DocumentCollection documentCollection, System.Linq.Expressions.Expression<Func<T, bool>> predicate) where T : IDocument
        {
            return client.CreateDocumentQuery<T>(documentCollection.DocumentsLink)
                   .Where(predicate)
                  .AsEnumerable();
        }

        /// <summary>
        /// Update Document Async
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="SelfLink"></param>
        /// <param name="document"></param>
        /// <param name="requestOptions"></param>
        /// <returns></returns>
        public async Task UpdateDocumentAsync<T>(string SelfLink, T document, RequestOptions requestOptions = null) where T : IDocument
        {
            await client.ReplaceDocumentAsync(SelfLink, document, requestOptions);
        }

        /// <summary>
        /// Delete Document Async
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="SelfLink"></param>
        /// <param name="document"></param>
        /// <param name="requestOptions"></param>
        /// <returns></returns>
        public async Task DeleteDocumentAsync<T>(string SelfLink, T document, RequestOptions requestOptions = null) where T : IDocument
        {
            await client.DeleteDocumentAsync(SelfLink, requestOptions);
        }

        /// <summary>
        /// Delete database if exist
        /// </summary>
        /// <param name="databaseId"></param>
        public void DeleteDatabase(string databaseId)
        {
            Database db = client.CreateDatabaseQuery()
                             .Where(d => d.Id == databaseId)
                             .AsEnumerable()
                             .FirstOrDefault();

            if (db == null)
            {
                client.DeleteDatabaseAsync(db.SelfLink).Wait();
            }
        }
        /// <summary>
        /// Register trigger to a collection    
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public async Task RegisterTriggerAsync(DocumentCollection collection, Trigger trigger)
        {
            await client.CreateTriggerAsync(collection.SelfLink, trigger);
        }
        /// <summary>
        /// Get registered triggers for specific collection
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public IEnumerable<Trigger> GetRegisteredTriggersForCollection(DocumentCollection collection)
        {
            return client.CreateTriggerQuery(collection.SelfLink);
        }
        #endregion

    }
}
