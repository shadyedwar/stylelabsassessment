﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    public interface IDirtyTrackable
    {
        /// <summary>
        /// Gets a value indicating whether this instance is tracking.
        /// </summary>
        [JsonIgnore]
        bool IsTracking { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is dirty.
        /// </summary>
        [JsonIgnore]
        bool IsDirty { get; }

        /// <summary>
        /// Starts tracking dirtyness.
        /// </summary>
        void StartTracking();
    }
}
