﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    /// <summary>
    /// Manage document Ids
    /// </summary>
    public interface IDocument
    {
        /// <summary>
        /// Auto generated Id
        /// </summary>
        long Id { get;  }

        /// <summary>
        /// String representation of Auto generated I, it will be used by DocumentDB
        /// </summary>
        string StringId { get;  }
       
    }
}
