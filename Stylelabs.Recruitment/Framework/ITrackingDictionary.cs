﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    /// <summary>
    /// A change-tracking enabled dictionary.
    /// </summary>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface ITrackingDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IDirtyTrackable
    {
    }
}
