﻿using Microsoft.Azure.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{

    public interface IRepository<T>
    {
        /// <summary>
        /// Add Documet
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>T WHere T implements IDocument</returns>
        T Add(T obj);

        /// <summary>
        /// Delete Document
        /// </summary>
        /// <param name="Id"></param>
        void Delete(T Id);

        /// <summary>
        /// Update Document
        /// </summary>
        /// <param name="obj"></param>
        void Update(T obj);

        /// <summary>
        /// Delete All Documents Without Firing Triggers
        /// </summary>
        void DeleteAllDocumentsWithoutTriggers();

        /// <summary>
        ///  Get dynamic Items/item by linq expression
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>T WHere IEnumerable<T> implements IDocument</returns>
        IEnumerable<T> GetItem(Expression<Func<T, bool>> predicate);

        /// <summary>
        ///  Get dynamic Items/item by linq expression, it will be used whenever we need to get a SelfLink for a specific document
        ///  Mainly use in Delete and Update operations.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IEnumerable<dynamic> GetItemDynamic(System.Linq.Expressions.Expression<Func<Document, bool>> predicate);
    }
}
