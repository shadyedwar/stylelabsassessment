﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    /// <summary>
    /// A scored set associates a number of unique items with scores.
    /// </summary>
    /// <typeparam name="T">Data-type of the set.</typeparam>
    public interface IScoredSet<T> : IReadOnlyList<T>
    {
        bool Add(T item, int score);
        int ScoreOf(T item);
    }
}
