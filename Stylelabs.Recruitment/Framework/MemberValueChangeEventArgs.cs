﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    public class MemberValueChangeEventArgs : EventArgs
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name of the property of which the value changed.
        /// </summary>
        public string MemberName { get; private set; }

        #endregion

        #region Constructors

        public MemberValueChangeEventArgs(string memberName)
        {
            MemberName = memberName;
        }

        #endregion
    }
}
