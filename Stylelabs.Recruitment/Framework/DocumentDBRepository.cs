﻿using System;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System.Collections.Generic;
using System.Linq;

namespace Stylelabs.Recruitment.Framework
{
   
    public class DocumentDBRepository<T> : IRepository<T> where T: IDocument
    {
        #region Fields
        private DocumentCollection collection;
        private IContext context;
        private string collectionId;
        #endregion

        #region Constructors
        public DocumentDBRepository(IContext context, string collectionId)
        {
            this.collectionId = collectionId;
            this.context = context;
            collection = context.ReadOrCreateCollection(collectionId);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get registered triggers for a collection
        /// </summary>
        /// <param name="type"></param>
        /// <param name="operation"></param>
        /// <returns>list of triggers</returns>
        private List<string> GetTriggers(TriggerType type, TriggerOperation operation)
        {
            return context.GetRegisteredTriggersForCollection(collection).ToList<Trigger>().Where(t => t.TriggerType == type && t.TriggerOperation == operation).Select(t => t.Id).ToList<string>();
        }
        /// <summary>
        /// Delete document eithout firing any trigger
        /// </summary>
        /// <param name="obj"></param>
        private void DeleteDocumentWithoutTriggers(T obj)
        {
            try
            {
                var doc = GetItemDynamic(item => item.Id == obj.StringId).SingleOrDefault();
                context.DeleteDocumentAsync(doc.SelfLink, obj).Wait();
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation of IRepository

        /// <summary>
        /// Add document
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public T Add(T obj)
        {
            try
            {
                var response = context.AddDocumentAsync<T>(collection, obj, new RequestOptions { PreTriggerInclude = GetTriggers(TriggerType.Pre, TriggerOperation.Create), PostTriggerInclude = GetTriggers(TriggerType.Post, TriggerOperation.Create) }).Result;
                return response;
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Delete Document
        /// </summary>
        /// <param name="obj"></param>
        public void Delete(T obj)
        {
            try
            {
                var doc = GetItemDynamic(item => item.Id == obj.StringId).SingleOrDefault(); // using this object to get document SelfLink
                context.DeleteDocumentAsync(doc.SelfLink, obj, new RequestOptions { PreTriggerInclude = GetTriggers(TriggerType.Pre, TriggerOperation.Delete), PostTriggerInclude = GetTriggers(TriggerType.Post, TriggerOperation.Delete) }).Wait();
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete All documents eithout firing any trigger
        /// </summary>
        public void DeleteAllDocumentsWithoutTriggers()
        {
            var docs = GetItem(doc => true).ToList<T>();
            foreach (T doc in docs)
            {
                DeleteDocumentWithoutTriggers(doc);
            }
        }

        /// <summary>
        /// uodate documet
        /// </summary>
        /// <param name="obj"></param>
        public void Update(T obj)
        {
            try
            {
                var doc = GetItemDynamic(item => item.Id == obj.StringId).SingleOrDefault();
                context.UpdateDocumentAsync(doc.SelfLink, obj, new RequestOptions {PreTriggerInclude = GetTriggers(TriggerType.Pre, TriggerOperation.Replace), PostTriggerInclude = GetTriggers(TriggerType.Post, TriggerOperation.Replace) }).Wait();
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Get  Items/item by linq expression
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<T> GetItem(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            try
            {
                return context.GetItem(collection, predicate).ToList<T>();
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Get dynamic Items/item by linq expression, it will be used whenever we need to get a SelfLink for a specific document
        ///  Mainly use in Delete and Update operations
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<dynamic> GetItemDynamic(System.Linq.Expressions.Expression<Func<Document, bool>> predicate)
        {
            try
            {
                return context.GetItemDynamic(collection, predicate);
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
