﻿//Chicking referential integrity during Update  document, 
///Rules: 1- can't Update document refrenced by other entities.
///       2- can't add a relation to non exist Entity Definition

function CheckReferentialIntegrityForUpdate() {

    var context = getContext();
    var collection = context.getCollection();
    var request = context.getRequest();
    var documentToUpdate = request.getBody();

    // retrive Current Document Associated Entity Definition Ids
    var currentEntityDefinitionId = documentToUpdate["DId"]


    //Get All Entities which are refrencing current Entity definition Id .
    collection.queryDocuments(collection.getSelfLink(), 'SELECT value EC.DefinitionId FROM EntityCollection EC where EC.DefinitionId = ' + currentEntityDefinitionId, {},
      function (err, entitiesDefinitionIds, responseOptions) {

          if (entitiesDefinitionIds.length > 0) {
              throw new Error("Trying to delete entity referenced by other Entities or EntityDefinitions");//Throw error if AssociatedEntityId  not eist.
          }

      });
      

    //Retrive Current Document Associated Entity Definition Ids
    var CurrentDocumentAssociatedEntityIds = [];
    documentToUpdate['MemberGroups'].forEach(function (value) {
        value.MemberDefinitions['$values'].filter(function (value2) {
            return value2['AssociatedEntityDefinitionId'] != null;
        }).forEach(function (value3) {
            CurrentDocumentAssociatedEntityIds.push(value3['AssociatedEntityDefinitionId']);
        });
    });
    //Get All Entity Definition Ids 
    collection.queryDocuments(collection.getSelfLink(), 'SELECT value c.DId FROM c', {},

      function (err, documentIds, responseOptions) {

          //Check if each elment in CurrentDocumentAssociatedEntityIds exist in documentIds
          CurrentDocumentAssociatedEntityIds.forEach(function (value) {

              if (documentIds.indexOf(value) == -1)
              {
                  throw new Error("Can't reference to entity not exist.");//Throw error if AssociatedEntityId  not eist.
              }

          });

      });
    request.setBody(documentToUpdate);
};
