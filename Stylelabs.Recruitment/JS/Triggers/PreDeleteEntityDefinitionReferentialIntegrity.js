﻿//Chicking referential integrity during delete  document, Shouldn't delete Entity Def which are being referenced by other EntityDefs or Entities.

function CheckReferentialIntegrityForDelete() {

    var context = getContext();
    var collection = context.getCollection();
    var request = context.getRequest();
    var documentToDelete = request.getBody();

    // retrive Current Document Associated Entity Definition Ids
    var currentEntityDefinitionId = documentToDelete["DId"]

    //Get All Entity Definition Ids which are refrencing current Entity definition Id .
    collection.queryDocuments(collection.getSelfLink(), 'SELECT value d.AssociatedEntityDefinitionId FROM  c JOIN m IN c.MemberGroups JOIN d IN  m.MemberDefinitions[\'$values\'] where  d.AssociatedEntityDefinitionId = ' + currentEntityDefinitionId, {},

      function (err, AssociatedEntityDefinitionIds, responseOptions) {

          if (AssociatedEntityDefinitionIds.length > 0)
          {
             
              throw new Error("Trying to delete entity referenced by other Entities or EntityDefinitions" );//Throw error if currentEntityDefinitionId is referenced by any oth Entity Definitions .
          }

      });

    //Get All Entities which are refrencing current Entity definition Id .
    collection.queryDocuments(collection.getSelfLink(), 'SELECT value EC.DefinitionId FROM EntityCollection EC where EC.DefinitionId = ' + currentEntityDefinitionId, {},
      function (err, entitiesDefinitionIds, responseOptions) {

          if (entitiesDefinitionIds.length > 0)
          {
              throw new Error("Trying to delete entity referenced by other Entities or EntityDefinitions");//Throw error if AssociatedEntityId  not eist.
          }

      });
      
    request.setBody(documentToDelete);
};
