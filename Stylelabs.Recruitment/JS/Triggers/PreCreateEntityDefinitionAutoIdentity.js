﻿
//This trigger will set auto increment identity starting from index 1 and 
//Checking referential integrity during create new document, you can't reference to document not exist
function SetAutoIdentity() {

    var context = getContext();
    var collection = context.getCollection();
    var request = context.getRequest();
    var documentToCreate = request.getBody(); // Read current document

    //Retrive Current Document Associated Entity Definition Ids
    var CurrentDocumentAssociatedEntityIds = [];
    documentToCreate['MemberGroups'].forEach(function (value) {
        value.MemberDefinitions['$values'].filter(function (value2) {
            return value2['AssociatedEntityDefinitionId'] != null;
        }).forEach(function (value3) {
            CurrentDocumentAssociatedEntityIds.push(value3['AssociatedEntityDefinitionId']);
        });
    });

    //Get All Entity Definition Ids in the collection
    collection.queryDocuments(collection.getSelfLink(), 'SELECT value c.DId FROM c', {},

      function (err, documentIds, responseOptions) {

          //Check if each elment in CurrentDocumentAssociatedEntityIds exist in documentIds
          CurrentDocumentAssociatedEntityIds.forEach(function (value) {

              if (documentIds.indexOf(value) == -1)
              {
                  throw new Error("Can't reference to entity not exist.");//Throw error if AssociatedEntityId  not eist.
              }

          });

      });

    //Generate Auto ID
    collection.queryDocuments(collection.getSelfLink(), 'SELECT VALUE c.DId FROM c', {},

       function (err, documents, responseOptions) {

           if (documents.length > 0) {

               var max = Math.max.apply(Math, documents); // find max

               documentToCreate["DId"] = max + 1;
           }
           else {
               documentToCreate["DId"] =  1;
           }
           documentToCreate["id"] = documentToCreate["DId"].toString();
          
       });
    request.setBody(documentToCreate);
};
