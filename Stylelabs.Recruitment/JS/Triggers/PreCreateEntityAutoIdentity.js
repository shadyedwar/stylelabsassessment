﻿
//This trigger will set auto increment identity starting from index 1 and 

function SetAutoIdentity() {

    var context = getContext();
    var collection = context.getCollection();
    var request = context.getRequest();
    var documentToCreate = request.getBody(); // Read current document

    //Generate Auto ID
    collection.queryDocuments(collection.getSelfLink(), 'SELECT VALUE c.DId FROM c', {},

       function (err, documents, responseOptions) {

           if (documents.length > 0) {

               var max = Math.max.apply(Math, documents); // find max

               documentToCreate["DId"] = max + 1;
           }
           else {
               documentToCreate["DId"] =  1;
           }
           documentToCreate["id"] = documentToCreate["DId"].toString();
          
       });
    request.setBody(documentToCreate);
};
