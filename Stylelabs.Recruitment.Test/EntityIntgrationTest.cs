﻿using System;
using Autofac;
using NUnit.Framework;
using Stylelabs.Recruitment.Entities;
using Stylelabs.Recruitment.Framework;
using System.Globalization;
using System.Linq;

namespace Stylelabs.Recruitment.Test
{

    /// <summary>
    ///     /// <summary>
    /// Teting: CURD operations on Entity.
    ///        
    /// </summary>
    /// </summary>
    [TestFixture]
    public class EntityIntgrationTest
    {
        private IRepository<EntityDefinition> entityDefRepo;
        private EntityDefinition movieDefinition;
        private EntityDefinition actorDefinition;
        private IRepository<Entity> entityRepo;
        private IEntityFactory entityFactory;
        private CultureInfo culture;

        [TestFixtureSetUp]
        public void setup()
        {
            Application.Startup("TestDB");
            entityDefRepo = Application.Container.Resolve<IRepository<EntityDefinition>>();
            entityRepo = Application.Container.Resolve<IRepository<Entity>>();
            entityFactory = Application.Container.Resolve<IEntityFactory>();
            culture = CultureInfo.GetCultureInfo("en-US");
            entityDefRepo.DeleteAllDocumentsWithoutTriggers();// Clean data
            movieDefinition = entityDefRepo.Add(TestHelper.GetMovieDefinition()); // Add MovieDefinition
            actorDefinition = entityDefRepo.Add(TestHelper.GetActorDefinition());// Add ActorDefinition

        }
        [SetUp]
        public void setupBeforeAnyUnitTest()
        {
            entityRepo.DeleteAllDocumentsWithoutTriggers(); //Claean data in the collection
        }

        /// <summary>
        /// Testing Add Operation and Auto identity
        /// </summary>
        [Test]
        public void ShouldSaveEntitiesAndPopulateIds()
        {
            var movie = entityFactory.Create(movieDefinition, culture);
            movie.GetProperty<string>("Title").Value = new PropertyValue<string>("My Movie");
            movie.GetProperty<DateTime>("ReleaseDate").Value = new PropertyValue<DateTime>(DateTime.Now);

            var actor1 = entityFactory.Create(actorDefinition, culture);
            actor1.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 1");

            var actor2 = entityFactory.Create(actorDefinition, culture);
            actor2.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 2");

            movie = entityRepo.Add(movie);
            actor1 = entityRepo.Add(actor1);
            actor2 = entityRepo.Add(actor2);


            Assert.AreEqual(1, movie.Id);
            Assert.AreEqual(2, actor1.Id);
            Assert.AreEqual(3, actor2.Id);
        }

        /// <summary>
        /// Testing retrieve operation
        /// </summary>
        [Test]
        public void ShouldRetrieveEntitiesCorrectly()
        {
            DateTime d = DateTime.Now;
            var movie = entityFactory.Create(movieDefinition, culture);
            movie.GetProperty<string>("Title").Value = new PropertyValue<string>("My Movie");
            movie.GetProperty<DateTime>("ReleaseDate").Value = new PropertyValue<DateTime>(d);

            var actor1 = entityFactory.Create(actorDefinition, culture);
            actor1.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 1");

            entityRepo.Add(movie);
            entityRepo.Add(actor1);
            movie = entityRepo.GetItem(item => item.Id == 1).SingleOrDefault();
            actor1 = entityRepo.GetItem(item => item.Id == 2).SingleOrDefault();

            Assert.AreEqual(1, movie.Id);
            Assert.AreEqual(2, actor1.Id);
            Assert.AreEqual("Actor 1",  actor1.GetProperty<string>("Name").Value.Value);
            Assert.AreEqual("My Movie", movie.GetProperty<string>("Title").Value.Value);
            Assert.AreEqual(d, movie.GetProperty<DateTime>("ReleaseDate").Value.Value);
        }

          /// <summary>
        /// Testing update operation
        /// </summary>
        [Test]
        public void ShoudUpdateEntityCorrectly()
        {

            DateTime d = DateTime.Now;
            var movie = entityFactory.Create(movieDefinition, culture);
            movie.GetProperty<string>("Title").Value = new PropertyValue<string>("My Movie");
            movie.GetProperty<DateTime>("ReleaseDate").Value = new PropertyValue<DateTime>(d);

            var actor1 = entityFactory.Create(actorDefinition, culture);
            actor1.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 1");

            //save movie and actor
            entityRepo.Add(movie);
            entityRepo.Add(actor1);
            movie = entityRepo.GetItem(item => item.Id == 1).SingleOrDefault();
            actor1 = entityRepo.GetItem(item => item.Id == 2).SingleOrDefault();

            //Update movie and actor
            d = DateTime.Now;
            movie.GetProperty<string>("Title").Value = new PropertyValue<string>("updated Movie");
            movie.GetProperty<DateTime>("ReleaseDate").Value = new PropertyValue<DateTime>(d);
            actor1.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 2");

            entityRepo.Update(movie);
            entityRepo.Update(actor1);

            //assert updated values
            Assert.AreEqual(1, movie.Id);
            Assert.AreEqual(2, actor1.Id);
            Assert.AreEqual("Actor 2", actor1.GetProperty<string>("Name").Value.Value);
            Assert.AreEqual("updated Movie", movie.GetProperty<string>("Title").Value.Value);
            Assert.AreEqual(d, movie.GetProperty<DateTime>("ReleaseDate").Value.Value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void ShoudDeleteEntityCorrectly()
        {
            var movie = entityFactory.Create(movieDefinition, culture);
            movie.GetProperty<string>("Title").Value = new PropertyValue<string>("My Movie");
            movie.GetProperty<DateTime>("ReleaseDate").Value = new PropertyValue<DateTime>(DateTime.Now);
            //save movie and actor
            movie = entityRepo.Add(movie);

            //delete entity
            entityRepo.Delete(movie);

            //get entity from db
            movie = entityRepo.GetItem(item => item.Id == 1).SingleOrDefault();
            //assert 
            Assert.IsNull(movie);
        }

        [TearDown]
        public void Clean()
        {
            entityRepo.DeleteAllDocumentsWithoutTriggers(); ///Claean data in the collection
        }
    }


}
