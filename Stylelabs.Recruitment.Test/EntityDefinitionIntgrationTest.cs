﻿using System;
using Autofac;
using NUnit.Framework;
using Stylelabs.Recruitment.Entities;
using Stylelabs.Recruitment.Framework;
using System.Globalization;
using System.Linq;
namespace Stylelabs.Recruitment.Test
{
    /// <summary>
    /// Teting: 1-CURD operations on EntityDefinition.
    ///         2- Addig new DataTime property to Movie Def.
    ///         3- Referential Integrity
    /// </summary>
    [TestFixture]
    public class EntityDefinitionIntgrationTest
    {

        IRepository<EntityDefinition> repo;
        [TestFixtureSetUp]
        public void setup()
        {
            Application.Startup("TestDB");
            repo = Application.Container.Resolve<IRepository<EntityDefinition>>();
        }
        [SetUp]
        public void setupBeforeAnyUnitTest()
        {
            repo.DeleteAllDocumentsWithoutTriggers(); //Claean data in a collection

        } 
        /// <summary>
        /// Should successfully add documents to collection and Ids auto incremented
        /// </summary>
        [Test]
        public void ShouldAddDocumentsToCollectionAndIdsAutoIncrement()
        {

            var movie = repo.Add(TestHelper.GetMovieDefinition()); // Add MovieDefinition
            var actor = repo.Add(TestHelper.GetActorDefinition());// Add Actor
            Assert.AreEqual(1, movie.Id);
            Assert.AreEqual(2, actor.Id);

        }

        /// <summary>
        /// Testing Adding New Date Field
        /// </summary>
        [Test]
        public void ShoudAddNewDateTimeFieldCorrectly()
        {
            var movie = TestHelper.GetMovieDefinition(); // Add MovieDefinition

            //Add new New Date Field
            movie.MemberGroups[0].MemberDefinitions.Add(new DateTimePropertyDefinition
                                {
                                    Name = "NewDateField",
                                    IsMandatory = true,
                                    Labels = { { CultureInfo.GetCultureInfo("en-US"), "New Date Field" } }
                                });
           repo.Add(movie);
           movie = repo.GetItem(doc => doc.Id == 1).SingleOrDefault(); // Retrive movie from database

            var newDateField =  movie.MemberGroups[0].MemberDefinitions.Where(m=>m.Name == "NewDateField").SingleOrDefault();
            //Assert Movie Data
            Assert.IsNotNull(newDateField);
            Assert.AreEqual(4, movie.MemberGroups[0].MemberDefinitions.Count);

        }

        /// <summary>
        /// Testing retrive data opertation
        /// </summary>
        [Test]
        public void ShoudRetrieveEntityDefinitionCorrectly()
        {
            repo.Add(TestHelper.GetMovieDefinition()); // Add MovieDefinition
            repo.Add(TestHelper.GetActorDefinition());// Add Actor

            var movie = repo.GetItem(doc => doc.Id == 1).SingleOrDefault(); // Retrive movie from database
            var actor = repo.GetItem(doc => doc.Id == 2).SingleOrDefault(); // Retrive movie from database

            //Assert Movie Data
            Assert.IsNotNull(movie);
            Assert.AreEqual("Movie", movie.Name);
            Assert.IsNotEmpty(movie.MemberGroups[0].MemberDefinitions);
            Assert.AreEqual(3, movie.MemberGroups[0].MemberDefinitions.Count);

            //Assert Actor Data
            Assert.IsNotNull(actor);
            Assert.AreEqual("Actor", actor.Name);
            Assert.IsNotEmpty(actor.MemberGroups[0].MemberDefinitions);
            Assert.AreEqual(1, actor.MemberGroups[0].MemberDefinitions.Count);
        }

        /// <summary>
        /// Testing delete data opertation
        /// </summary>
        [Test]
        public void ShoudDeleteEntityDefinitionsCorrectly()
        {
            repo.Add(TestHelper.GetMovieDefinition()); // Add MovieDefinition
            var movie = repo.GetItem(doc => doc.Id == 1).SingleOrDefault(); // Retrive movie from database

             
            repo.Delete(movie); //Delete movie 
            movie = repo.GetItem(doc => doc.Id == 1).SingleOrDefault(); 

            Assert.IsNull(movie);
        }

        /// <summary>
        /// Testing Update data opertation
        /// </summary>
         [Test]
        public void ShoudUpdateEntityDefinitionsCorrectly()
        {
            repo.Add(TestHelper.GetMovieDefinition()); // Add MovieDefinition
            var movie = repo.GetItem(doc => doc.Id == 1).SingleOrDefault<EntityDefinition>(); // Retrive movie from database

            movie.Name = "Movie1";
           movie.MemberGroups[0].MemberDefinitions.RemoveAt(1); // update Member Definitions 
        


            repo.Update(movie); //Update  movie 
            movie = repo.GetItem(doc => doc.Id == 1).SingleOrDefault<EntityDefinition>();

            Assert.AreEqual("Movie1", movie.Name);
            Assert.AreEqual(2, movie.MemberGroups[0].MemberDefinitions.Count);
        }

        /// <summary>
        /// Testing setup many to many relationship between movie and actor
        /// </summary>
        [Test]
        public void ShoudSetUpAndSavedrelationshipCorrectly()
        {
            repo.Add(TestHelper.GetMovieDefinition()); // Add MovieDefinition
            repo.Add(TestHelper.GetActorDefinition());// Add ActorDefinition

            var movie = repo.GetItem(doc => doc.Id == 1).SingleOrDefault(); // Retrive movie from database
            var actor = repo.GetItem(doc => doc.Id == 2).SingleOrDefault(); // Retrive movie from database

            //Setup many to many relationship between Movie and Actor
            movie.MemberGroups[0].MemberDefinitions.Add(new RelationDefinition()
            {
                AssociatedEntityDefinitionId = actor.Id,
                Cardinality = RelationCardinality.OneToMany,
            });
            actor.MemberGroups[0].MemberDefinitions.Add(new RelationDefinition()
            {
                AssociatedEntityDefinitionId = movie.Id,
                Cardinality = RelationCardinality.OneToMany,
            });

            //Save relationship
            repo.Update(movie);
            repo.Update(actor);

            //Asert relationship 
             movie = repo.GetItem(doc => doc.Id == 1).SingleOrDefault(); 
             actor = repo.GetItem(doc => doc.Id == 2).SingleOrDefault();
             foreach (var currentMemberDef in movie.MemberGroups[0].MemberDefinitions)
             {
                 RelationDefinition relDef = currentMemberDef as RelationDefinition;
                 if (relDef != null)
                 {
                     Assert.AreEqual(2, relDef.AssociatedEntityDefinitionId);
                     Assert.AreEqual(RelationCardinality.OneToMany, relDef.Cardinality);
                 }
             }
             foreach (var currentMemberDef in actor.MemberGroups[0].MemberDefinitions)
             {
                 RelationDefinition relDef = currentMemberDef as RelationDefinition;
                 if (relDef != null)
                 {
                     Assert.AreEqual(1, relDef.AssociatedEntityDefinitionId);
                     Assert.AreEqual(RelationCardinality.OneToMany, relDef.Cardinality);
                 }
             }
           
        }

        /// <summary>
        /// Testing referential integrity for delete operation , Shouldn't delete Entity Def which are being referenced by another Entity Def
        /// </summary>
        [Test]
        public void ShouldNotDleteEntityDefReferencedByAnotherEntityDefs()
        {
            repo.Add(TestHelper.GetMovieDefinition()); // Add MovieDefinition
            repo.Add(TestHelper.GetActorDefinition());// Add ActorDefinition

            var movie = repo.GetItem(doc => doc.Id == 1).SingleOrDefault(); // Retrive movie from database
            var actor = repo.GetItem(doc => doc.Id == 2).SingleOrDefault(); // Retrive movie from database

            //Setup many to many relationship between Movie and Actor
            movie.MemberGroups[0].MemberDefinitions.Add(new RelationDefinition()
            {
                AssociatedEntityDefinitionId = actor.Id,
                Cardinality = RelationCardinality.OneToMany,
            });
            actor.MemberGroups[0].MemberDefinitions.Add(new RelationDefinition()
            {
                AssociatedEntityDefinitionId = movie.Id,
                Cardinality = RelationCardinality.OneToMany,
            });

            //Save relationship
            repo.Update(movie);
            repo.Update(actor);

            try
            {
              repo.Delete(movie);
              Assert.Fail("No Exception is thrown");
            }
            catch (AggregateException ex)
            {
                Assert.True(true);
            }
            catch (ArgumentException ex)
            {
                Assert.True(true);
            }
            catch (Exception ex)
            {
                Assert.True(true);
            }
           
        }

        /// <summary>
        /// Testing referential integrity for create operation , Shouldn't create entityDef with reference to non existing EntityDef
        /// </summary>
        [Test]
        public void ShouldNotCreateEntityDefWithReferenceToNonExistingEntityDef()
        {
            var movie = TestHelper.GetMovieDefinition(); // Add MovieDefinition

            //Setup many to many relationship between Movie and Actor
            movie.MemberGroups[0].MemberDefinitions.Add(new RelationDefinition()
            {
                AssociatedEntityDefinitionId = 10,
                Cardinality = RelationCardinality.OneToMany,
            });
          

            try
            {
                repo.Add(movie);
                Assert.Fail("No Exception is thrown");
            }
            catch (AggregateException ae)
            {
                Assert.True(true);
            }
            catch (ArgumentException ex)
            {
                Assert.True(true);
            }
            catch (Exception ex)
            {
                Assert.True(true);
            }

        }


        /// <summary>
        /// Testing referential integrity for update operation , Shouldn't update entityDef with reference to non existing EntityDef
        /// </summary>
        [Test]
        public void ShouldNotUpdateEntityDefWithReferenceToNonExistingEntityDef()
        {
            repo.Add(TestHelper.GetMovieDefinition()); // Add MovieDefinition
            var movie = repo.GetItem(doc => doc.Id == 1).SingleOrDefault(); // Retrive movie from database
            //Setup many to many relationship between Movie and Actor
            movie.MemberGroups[0].MemberDefinitions.Add(new RelationDefinition()
            {
                AssociatedEntityDefinitionId = 10,
                Cardinality = RelationCardinality.OneToMany,
            });
           
            try
            {
                repo.Update(movie);
                Assert.Fail("No Exception is thrown");
            }
            catch (AggregateException ae)
            {
                Assert.True(true);
            }
            catch (ArgumentException ex)
            {
                Assert.True(true);
            }
            catch (Exception ex)
            {
                Assert.True(true);
            }

        }
        [TearDown]
        public void Clean()
        {
            repo.DeleteAllDocumentsWithoutTriggers();//Claean data in the collection
        }
    }


}
