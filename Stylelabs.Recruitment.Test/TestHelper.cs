﻿using Autofac;
using Stylelabs.Recruitment.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Test
{
    public class TestHelper
    {
        public static CultureInfo culture = CultureInfo.GetCultureInfo("en-US");
        /// <summary>
        /// Get Entity Definition dummy object
        /// </summary>
        /// <returns></returns>
        public static EntityDefinition GetMovieDefinition()
        {
            return new EntityDefinition
             {

                 Name = "Movie",
                 Labels = { 
                        new KeyValuePair<CultureInfo, string>(culture, "Movie Definition")
                    },
                 MemberGroups =
                    {
                        new MemberGroup
                        {
                            Name = "General",
                            MemberDefinitions =
                            {
                                new StringPropertyDefinition
                                {
                                    Name = "Title",
                                    IsMandatory = true,
                                    Labels = { { culture, "Title" } }
                                },
                                new DateTimePropertyDefinition
                                {
                                    Name = "ReleaseDate",
                                    IsMandatory = true,
                                    Labels = { { culture, "Release Date" } }
                                },
                                new StringPropertyDefinition
                                {
                                    Name = "StoryLine",
                                    Labels = { { culture, "StoryLine" } }
                                }
                            }
                         }
                    }
             };

        }

        /// <summary>
        /// Get Entity dummy object
        /// </summary>
        /// <returns></returns>
        public static EntityDefinition GetActorDefinition()
        {
            return new EntityDefinition
            {
                Name = "Actor",
                Labels = { 
                            new KeyValuePair<CultureInfo, string>(culture, "Actor Definition")
                        },
                MemberGroups =
                        {
                            new MemberGroup
                            {
                                Name = "General",
                                MemberDefinitions =
                                {
                                    new StringPropertyDefinition
                                    {
                                        Name = "Name",
                                        IsMandatory = true,
                                        Labels = { { culture, "Name" } }
                                    }
                                }
                            }
                        }
            };
        }
    }
}
