﻿using Stylelabs.Recruitment.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Stylelabs.Recruitment.Entities;
using System.Globalization;

namespace Stylelabs.Recruitment.DynamoDb.App
{
    public class Program
    {
        private static IRepository<EntityDefinition> entityDefRepo;
        private static EntityDefinition movieDefinition;
        private static EntityDefinition actorDefinition;
        private static IRepository<Entity> entityRepo;
        private static IEntityFactory entityFactory;
        private static CultureInfo culture;
        private static Entity movie;
        private static Entity actor1;
        private static Entity actor2;
        static void Main(string[] args)
        {
            // Wire classes
            Application.Startup();

            //resolve Repositories
            entityDefRepo = Application.Container.Resolve<IRepository<EntityDefinition>>();
            entityRepo = Application.Container.Resolve<IRepository<Entity>>();

            //Clean data before start
            entityDefRepo.DeleteAllDocumentsWithoutTriggers();
            entityRepo.DeleteAllDocumentsWithoutTriggers();

            // Get default culture
            culture = CultureInfo.GetCultureInfo("en-US");
            try
            {
                // Create domain model consisting of a movie and actor definition
                movieDefinition = new EntityDefinition
                {
                    Name = "Movie",
                    Labels = { 
                        new KeyValuePair<CultureInfo, string>(culture, "Movie Definition")
                    },
                    MemberGroups =
                    {
                        new MemberGroup
                        {
                            Name = "General",
                            MemberDefinitions =
                            {
                                new StringPropertyDefinition
                                {
                                    Name = "Title",
                                    IsMandatory = true,
                                    Labels = { { culture, "Title" } }
                                },
                                new DateTimePropertyDefinition
                                {
                                    Name = "ReleaseDate",
                                    IsMandatory = true,
                                    Labels = { { culture, "Release Date" } }
                                },
                                 new DateTimePropertyDefinition // Adding New DateTime field
                                {
                                    Name = "NewDateFiled",
                                    IsMandatory = true,
                                    Labels = { { culture, "New Date Filed" } }
                                },
                                new StringPropertyDefinition
                                {
                                    Name = "StoryLine",
                                    Labels = { { culture, "StoryLine" } }
                                }
                            }
                         }
                    }
                };

                actorDefinition = new EntityDefinition
               {
                   Name = "Actor",
                   Labels = { 
                            new KeyValuePair<CultureInfo, string>(culture, "Actor Definition")
                        },
                   MemberGroups =
                        {
                            new MemberGroup
                            {
                                Name = "General",
                                MemberDefinitions =
                                {
                                    new StringPropertyDefinition
                                    {
                                        Name = "Name",
                                        IsMandatory = true,
                                        Labels = { { culture, "Name" } }
                                    }
                                }
                            }
                        }
               };


                //1-Persist EntityDefinition
                PersistEntityDefinition();
                /////////////////////////////////////////////////////////

                //2-Configure relationships
                movieDefinition.MemberGroups[0].MemberDefinitions.Add(new RelationDefinition()
                {
                    AssociatedEntityDefinitionId = actorDefinition.Id,
                    Cardinality = RelationCardinality.OneToMany,
                });
                actorDefinition.MemberGroups[0].MemberDefinitions.Add(new RelationDefinition()
                {
                    AssociatedEntityDefinitionId = movieDefinition.Id,
                    Cardinality = RelationCardinality.OneToMany,
                });
                ////////////////////////////////////////////////////////////

                //3-Persist relationships
                PersistEntityDefinitionRelationship();
                //////////////////////////////////////////////////////////////

                //4-Create entities fro defentions
                entityFactory = Application.Container.Resolve<IEntityFactory>();
                // Create a movie with two actors
                movie = entityFactory.Create(movieDefinition, culture);
                movie.GetProperty<string>("Title").Value = new PropertyValue<string>("My Movie");
                movie.GetProperty<DateTime>("ReleaseDate").Value = new PropertyValue<DateTime>(DateTime.Now);
                movie.GetProperty<DateTime>("NewDateFiled").Value = new PropertyValue<DateTime>(DateTime.Now);

                actor1 = entityFactory.Create(actorDefinition, culture);
                actor1.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 1");

                actor2 = entityFactory.Create(actorDefinition, culture);
                actor2.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 2");
                ///////////////////////////////////////////////////////////////////////////////////////

                //5-Persist Entities
                PersistEntity();
                ////////////////////////////////////////////////////////////////////////

                Console.WriteLine("Thank You");
                Console.ReadLine();
            }
            catch(AggregateException ae)
            {
                Console.WriteLine(ae.Message);
            }
            catch (ArgumentException ae)
            {
                Console.WriteLine(ae.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void PersistEntityDefinition()
        {
            movieDefinition = entityDefRepo.Add(movieDefinition);
            actorDefinition = entityDefRepo.Add(actorDefinition);
        }

        public static void PersistEntityDefinitionRelationship()
        {
            entityDefRepo.Update(movieDefinition);
            entityDefRepo.Update(actorDefinition);
        }

        public static void PersistEntity()
        {
            movie = entityRepo.Add(movie);
            actor1 = entityRepo.Add(actor1);
            actor2 = entityRepo.Add(actor2);
        }
    }
}
