First Thank you for gicing me chance to take the assessment
-------------------------------------------------
I spent from 7 to 8 hours  to solve it including learing time of DocumentDB  

---------------------------------------------
Accomplished tasks:
1- Able to persist EnitityDefentions and Entites 
2- Able to do GURD opertaions on EnitityDefentions and Entites 
3- Able to add new Datetime field and persist it
4- Able to configure and persist relationships
5- Able to implement Auto Increment Identity column using triggers
5- Able to do referential integrity check for Entity Definitions using tiggers (Explained below)
------------------------------------------------

Persisting Relationships:I thought in two ideas for persisting relationships:

First simple idea (it is the one which i have already implemented because it is simple -:)...) is as follow:
	1-Persist Entities/EntityDefinons first without relationships to generate Ids
	2-Configure and update  Entities/EntityDefinons relationshis
	3-Persist(update) Entities/EntityDefinons back to database 

   *Also i tried to implement some rules using triggers to keep data consistent and preserve referential integrity as much as i can such as:
	-You can't create EntityDef with a reference to non existing entity.
	-You can't update EntityDef which is being referenced by another EntityDef/Entities.
	-You can't delete EntityDef which is being referenced by another EntityDefs/Entities.

Second idea is to implement something like Entity Framework to DocumentDB which is enable you to keep tracks entities and generate one transaction to persist entities (like SaveChanges in Ef)
I am planning implement when i get any free time :)

--------------------------------------------------------------

Notes :
- I worte integration tests to cover most of cases and scenarios 
- I used default consistency and concurrency modes (still i am trying to figure out how isolation level can be used or implemented in DocumentDB)





I had so loaded sprint and urgent releases this sprint  so I am sorry for any delay or inaccuracy of my code implantation or my design deductions

Please let me know if you have any comments or questions.

Thank you
Shady Sadek